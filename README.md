# Block editor

## Task: 
Create a single page application for editing text blocks. Text blocks must contain text and type (h1, h2, p).

## Functionality:
1) Add a new text block
2) Update block type
3) Move block
4) HTML result should be rendered and updated automatically
