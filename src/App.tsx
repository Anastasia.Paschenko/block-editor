import { Header } from './Components/Header';
import { Editor } from './Components/Editor';
import styles from "./App.module.scss";
import { ResultViewer } from './Components/ResultViewer';

function App() {
  return (
    <div className={styles.app}>
      <Header />
      <div className={styles.container}>
        <div>
          <h2 className={styles.title}>Editor</h2>
          <Editor />
        </div>
        <div>
          <h2 className={styles.title}>Result</h2>
          <ResultViewer />
        </div>
      </div>
    </div>
  );
}

export default App;
