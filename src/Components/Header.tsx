import { FC } from "react"
import styles from "./Header.module.scss";

export const Header: FC = () => {
    return (
        <div className={styles.container}>
            <span className={styles.title}>Block editor</span>
        </div>
    )
}