import type { Identifier, XYCoord } from 'dnd-core'
import { FC, useEffect } from 'react'
import { useRef } from 'react'
import { useDrag, useDrop } from 'react-dnd'
import styles from "./Block.module.scss"
import { BlockTypeSelector } from './BlockTypeSelector'
import { observer } from 'mobx-react-lite'
import { store } from '../mobx/models'
import { BlockType } from '../Constants/enums'
import { blockTypeComponent, ITEM_TYPE } from '../Constants/constants'

interface BlockProps {
    id: any
    text: string
    index: number
    type: BlockType
}

interface DragItem {
    index: number
    id: string
    type: string
}

export const Block: FC<BlockProps> = observer(({ id, text, index, type }) => {
    const ref = useRef<HTMLDivElement>(null)
    const [{ handlerId }, drop] = useDrop<
        DragItem,
        void,
        { handlerId: Identifier | null }
    >({
        accept: ITEM_TYPE,
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            }
        },
        drop(item: DragItem, monitor) {
            if (!ref.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index

            if (dragIndex === hoverIndex) {
                return
            }

            const hoverBoundingRect = ref.current?.getBoundingClientRect()
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2
            const clientOffset = monitor.getClientOffset()
            const hoverClientY = (clientOffset as XYCoord).y - hoverBoundingRect.top

            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }

            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }

            store.moveBlock(dragIndex, hoverIndex)
            item.index = hoverIndex
        },
    })

    const [{ isDragging }, drag] = useDrag({
        type: ITEM_TYPE,
        item: () => {
            return { id, index }
        },
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging(),
        }),
    })

    useEffect(() => {
        drag(drop(ref))
    }, [ref])

    const onSelect = (value: BlockType) => {
        store.updateType({ index, type: value })
    }

    return (
        <div ref={ref} block-type={type} className={styles.block} data-handler-id={handlerId}>
            {blockTypeComponent[type](text)}
            <div className={styles.selectType}>
                <BlockTypeSelector onSelect={onSelect} value={type} />
            </div>
        </div>
    )
})
