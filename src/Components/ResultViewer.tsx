import { observer } from "mobx-react-lite"
import { getSnapshot } from "mobx-state-tree"
import { FC } from "react"
import { blockTypeHtml } from "../Constants/constants"
import { store } from "../mobx/models"
import styles from "./ResultViewer.module.scss";

export const ResultViewer: FC = observer(() => {
    return (
        <div className={styles.container}>
            {getSnapshot(store).blocks.map(({ text, type }, index) =>
                <div key={index}>{blockTypeHtml[type](text)}</div>)
            }
        </div>
    )
})