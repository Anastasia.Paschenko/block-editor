import { Select } from "antd"
import { FC } from "react"
import { BlockType } from "../Constants/enums"
const { Option } = Select

interface BlockTypeSelectorProps {
    onSelect: (value: BlockType) => void,
    value: BlockType | undefined,
}

export const BlockTypeSelector: FC<BlockTypeSelectorProps> = ({ onSelect, value }) => {
    return (
        <Select placeholder="Select" onChange={onSelect} value={value}>
            <Option value={BlockType.h1}>h1</Option>
            <Option value={BlockType.h2}>h2</Option>
            <Option value={BlockType.p}>p</Option>
        </Select>
    )
}