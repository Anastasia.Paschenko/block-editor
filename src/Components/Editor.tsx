import type { FC } from 'react'
import { useCallback } from 'react'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import { Block } from './Block'
import styles from "./Editor.module.scss";
import 'antd/dist/antd.css';
import { NewBlockComponent } from './NewBlockComponent'
import { store } from '../mobx/models'
import { getSnapshot } from 'mobx-state-tree'
import { observer } from 'mobx-react-lite'
import { BlockType } from '../Constants/enums'

interface BlockItem {
    id: number
    text: string
    type: BlockType
}

export const Editor: FC = observer(() => {
    const renderBlock = useCallback(
        (card: BlockItem, index: number) => {
            return (
                <Block
                    key={card.id}
                    index={index}
                    id={card.id}
                    text={card.text}
                    type={card.type}
                />
            )
        },
        [],
    )

    return (
        <>
            <div className={styles.container}>
                <DndProvider backend={HTML5Backend}>
                    {getSnapshot(store).blocks.map((block, i) => renderBlock(block, i))}
                </DndProvider>
            </div>
            <NewBlockComponent />
        </>
    )
})
