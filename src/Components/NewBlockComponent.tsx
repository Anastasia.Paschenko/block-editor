import { Input, Button } from "antd"
import { observer } from "mobx-react-lite"
import { FC, useState } from "react"
import { BlockType } from "../Constants/enums"
import { store } from "../mobx/models"
import { BlockTypeSelector } from "./BlockTypeSelector"

export const NewBlockComponent: FC = observer(() => {
    const [type, setType] = useState<BlockType>()
    const [text, setText] = useState<string>()

    const onSelect = (value: BlockType) => {
        setType(value)
    }

    const onInputChange = (e: any) => {
        setText(e.target.value)
    }

    const onAddClick = () => {
        if (type === undefined || !text) return

        store.addBlock({ type, text })
        setType(undefined)
        setText(undefined)
    }

    return (
        <div style={{ display: 'flex' }} >
            <BlockTypeSelector value={type} onSelect={onSelect} />
            <Input onChange={onInputChange} placeholder="Text" value={text} />
            <Button type="primary" disabled={type === undefined || !text} onClick={onAddClick}>
                +
            </Button>
        </div >
    )
})