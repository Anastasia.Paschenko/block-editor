import { types } from "mobx-state-tree"
import { BlockType } from "../Constants/enums";

const Block = types.model({
    id: types.number,
    text: types.string,
    type: types.enumeration(Object.values(BlockType)),
})

const RootStore = types.model({
    blocks: types.array(Block)
}).actions(self => ({
    addBlock({ text, type }: { text: string, type: BlockType }) {
        self.blocks.push({ id: self.blocks.length, text, type })
    },
    updateType({ index, type }: { index: number, type: BlockType }) {
        self.blocks[index].type = type
    },
    moveBlock(dragIndex: number, hoverIndex: number) {
        const element = Object.assign({}, self.blocks[dragIndex]);
        self.blocks.splice(dragIndex, 1)
        self.blocks.splice(hoverIndex, 0, element)
    }
}));

export const store = RootStore.create({
    blocks: []
});