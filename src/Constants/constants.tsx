import { BlockType } from "./enums"

export const ITEM_TYPE = 'block'

export const blockTypeComponent = {
    [BlockType.h1]: (children: any) => <h1>{children}</h1>,
    [BlockType.h2]: (children: any) => <h2>{children}</h2>,
    [BlockType.p]: (children: any) => <p>{children}</p>,
}

export const blockTypeHtml = {
    [BlockType.h1]: (children: any) => `<h1>${children}</h1>`,
    [BlockType.h2]: (children: any) => `<h2>${children}</h2>`,
    [BlockType.p]: (children: any) => `<p>${children}</p>`,
}